from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views

from . import views


router = routers.DefaultRouter()
router.register('favorite-books', views.FavoriteBookViewSet, basename='favorite-book')

urlpatterns = [
    path('', include(router.urls)),
    path('favorite-books/<int:book_id>/image/', views.FavoriteBookImageUploadView.as_view()),
    path('users/register/', views.RegisterView.as_view()),
    path('users/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('users/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
