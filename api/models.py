from django.contrib.auth.models import User
from django.db import models


class FavoriteBook(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='favorite_books')
    author = models.CharField(max_length=100, null=False)
    title = models.CharField(max_length=250, null=False)
    description = models.TextField(null=False)
    image = models.ImageField(null=True)
