from django.conf import settings
from django.contrib.auth.models import User, Group
from rest_framework import serializers

from api.models import FavoriteBook


class FavoriteBookSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    def get_image(self, book):
        return settings.MEDIA_URL + book.image.name if book.image else None

    class Meta:
        model = FavoriteBook
        fields = ['id', 'author', 'title', 'description', 'image']


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=20, write_only=True, required=True)
    password = serializers.CharField(max_length=50, write_only=True, required=True)

    def create(self, validated_data):
        try:
            user = User.objects.create_user(
                validated_data['username'], password=validated_data['password'])

            return user
        except Exception:
            raise serializers.ValidationError('Username already taken.')
