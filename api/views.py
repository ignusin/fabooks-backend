from rest_framework import permissions, views, viewsets, status
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response

from api.models import FavoriteBook
from api.serializers import FavoriteBookSerializer, RegisterSerializer


class FavoriteBookViewSet(viewsets.ModelViewSet):
    serializer_class = FavoriteBookSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return FavoriteBook.objects.filter(user=user).order_by('title')

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class FavoriteBookImageUploadView(views.APIView):
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = [FileUploadParser]

    def put(self, request, book_id):
        file_obj = request.data['file']

        book = FavoriteBook.objects.get(id=book_id)
        book.image = file_obj
        book.save()

        serializer = FavoriteBookSerializer(book)

        return Response(serializer.data, status=status.HTTP_200_OK)


class RegisterView(views.APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

        return Response(status=status.HTTP_200_OK)
